package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-redis/redis"
)

type Tracks []struct {
	Name string `json:"name"`
	Lat  string `json:"lat"`
	Lng  string `json:"lng"`
}

//type Tracks []Track

func main() {
	//rawData := http.Get(" https://api.motorsportreg.com/rest/calendars/organization/")

	http.HandleFunc("/tracks", getTracks)
	http.ListenAndServe(":80", nil)

	//getTracks()
}

//func getTracks() {
func getTracks(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)

	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	//Scan gets all the K/V pairs
	val1 := rdb.Scan(0, "*", 200).Iterator()

	trackData := Tracks{}

	//Loop through all the KV pairs and print the lat and long
	for val1.Next() {
		tracksResult, err := rdb.HGetAll(val1.Val()).Result()
		checkError(err)

		//fmt.Println(val1.Val(), trkInfo)

		//trackData := Tracks{
		//	{val1.Val(), trkInfo["lat"], trkInfo["long"]},
		//}

		//trackData := Tracks{}

		trackInfo := struct {
			Name string `json:"name"`
			Lat  string `json:"lat"`
			Lng  string `json:"lng"`
		}{
			val1.Val(),
			tracksResult["lat"],
			tracksResult["long"],
		}

		trackData = append(trackData, trackInfo)

		//fmt.Println(trackData)
	}
	w.Write(jsonConvert(trackData))
}

func jsonConvert(i interface{}) []byte {
	bs, err := json.Marshal(i)
	checkError(err)
	return bs
}

func checkError(e error) {
	if e != nil {
		fmt.Println(e)
	}
}
