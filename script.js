'use strict'

const eventsBtn = document.querySelector('.button1')
const tracksBtn = document.querySelector('.button2')
const backgGround = document.getElementsByTagName('body')
const popEvnticon = document.querySelector('.leaflet-popup-content-wrapper')

const popEvnticon1 = document.querySelector('.track-popup')
const autoxPopup = document.querySelector('.autox-popup')

const eventAutox = document.querySelector('.event--autox')
const eventTrackDay = document.querySelectorAll('.event--trackday')
const eventRace = document.querySelector('.event--race')

const eventSelection = document.querySelector('.event--selection')
const distanceSelection = document.querySelector('.distance--selection')
const calendar = document.querySelector('.calendar')

const overlay = document.querySelector('.overlay')
const modal = document.querySelector('.modal')

const trackpopups = document.querySelector('.leaflet-popup-content-wrapper')
const mapID = document.getElementById('map')

const btnClose = document.querySelector('.close-modal');




eventsBtn.addEventListener('click', function() {
    const app = new eventsApp()
})
tracksBtn.addEventListener('click', function() {
    const app = new tracksApp()
})


class eventInfo {

    constructor(type, title, organizer, date, location, coords, home) {
        this.type = type
        this.title = title
        this.organizer = organizer
        this.date = date
        this.location = location
        this.coords = coords
        this.home = home
    }

    distance() {
        var loc = L.latLng({
            lat: this.coords[0],
            lng: this.coords[1],
        })

        var homeLoc = L.latLng({
            lat: this.home[0],
            lng: this.home[1],
        })

        this.distance = homeLoc.distanceTo(loc)/1609.344
        return this.distance
    }

    id = (Math.random() + '').slice(13)
}

class eventsApp {
    //#map
    //#mapZoomlevel = 16
    map
    mapZoomlevel = 16
    events = []

    constructor() {
        this._getPosition()
        //this._loadMap()
        distanceSelection.addEventListener('change', this._distanceSelection.bind(this))
        eventSelection.addEventListener('change', this._toggleEvent.bind(this))
    }

    _getPosition() {
        const geoOptions = {
            enableHighAccuracy: true,
            timeout: 600000,
            maximumAge: Infinity,  
        }
        const loc = navigator.geolocation.getCurrentPosition(
            //position => console.log(position),
            this._loadMap.bind(this),
            function() {alert('Could not get your position')},
            geoOptions,)
    }

    _loadMap(position) {

        document.body.style.backgroundImage = "none"
        const {latitude} = position.coords
        const {longitude} = position.coords
        const coords = [latitude, longitude]
        const coords2 = [41.9295642,-73.3908371]
        const coords3 = [40.8135104,-74.0766457]
        const coords4 = [39.3589903,-75.0638023]

        const obj0 = L.latLng({
            lat: 40.8206198, 
            lng: -73.955992,
        })

        const obj1 = L.latLng({
            lat: 41.9295642, 
            lng: -73.3908371,
        })
       
        const obj2 = L.latLng({
            //coords3
            lat: 40.8135104, 
            lng: -74.0766457,
        })

        this.map = L.map('map').setView(coords, this.mapZoomlevel)
        
    
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(this.map);

        const group = new L.featureGroup([
            L.marker(coords2)
            .addTo(this.map)
            .bindPopup(
                L.popup({
                maxWidth: 250,
                minWidth: 100,
                autoClose: false,
                closeOnClick: true,
                closeButton: false,
                className: "track-popup",
                })
            )
            .setPopupContent("TrackDay")
            .openPopup(),
            L.marker(coords3)
            .addTo(this.map)
            .bindPopup(
                L.popup({
                maxWidth: 250,
                minWidth: 100,
                autoClose: false,
                closeOnClick: true,
                closeButton: false,
                className: "autox-popup",
                })
            )
            .setPopupContent("AutoCross")
            .openPopup(),
            L.marker(coords4)
            .addTo(this.map)
            .bindPopup(
                L.popup({
                maxWidth: 250,
                minWidth: 100,
                autoClose: false,
                closeOnClick: true,
                closeButton: false,
                className: "race-popup",
                })
            )
            .setPopupContent("Race")
            .openPopup(),
            //Set the home marker last to make the map load to that point -- may be a better way to do this
            L.marker(coords)
                .addTo(this.map)
                .bindPopup(
                    L.popup({
                    maxWidth: 250,
                    minWidth: 100,
                    autoClose: false,
                    closeOnClick: true,
                    closeButton: false,
                    closeButton: false,
                    })
                )
                .setPopupContent("Home")
                .openPopup()
        ])

        //const bounds = L.latLngBounds(group)
        //// sets the map zoon to include all markers on screen
        //this.map.fitBounds(group.getBounds())
        this.map.flyTo(coords, 9.5)

        console.log(coords)

       const meters = obj0.distanceTo(obj2)
       console.log("DISTANCE FROM HOME TO AUTOX in Miles",meters / 1609.344)

       const autox1 = new eventInfo('autox', 'NNJR Autocross Event #6 - Lot E', 'SCCA - Northern New Jersey Region', 'Sat, Jun 5, 2021', 'MetLife Stadium East Rutherford, NJ', [40.8135104, -74.0766457], coords)
       const autox2 = new eventInfo('autox', 'NYR SCCA Solo #3', 'SCCA - New York Region - Solo', 'Sun, Jun 6, 2021', 'Nassau County Veterans Memorial Col. Uniondale,, NY', [40.7232778,-73.5906528], coords)
       const autox3 = new eventInfo('autox', 'New York Region PSCC Autocross #3.21', 'SCCA - New York Region - PSCC', 'Sun, Jun 13, 2021', 'Tech City Lot Kingston, NY', [41.9701266,-73.9975338], coords)

       this.events.push(autox1)
       this.events.push(autox2)
       this.events.push(autox3)
  
       // arrow function works -- not normal syntax
       this.events.forEach(e => {
            //console.log(e.distance())
            //console.log(e.id)
            this._renderEvent(e)
            this._renderEventMarker(e)
       })
        
        function hideBtns() {
            eventsBtn.classList.add('buttonhide')
            tracksBtn.classList.add('buttonhide')
            setTimeout(() => eventsBtn.style.display ='none', 500)
            setTimeout(() => tracksBtn.style.display ='none', 500)
        }

        function unhideDrpdwn() {
            var dropdowns = document.querySelectorAll('.dropdown')
            dropdowns.forEach(function(el) {
                el.style.opacity='1' 
                el.style.transition='opacity 1.5s'
            })
        }

        hideBtns()
        unhideDrpdwn()     
    }

    _distanceSelection() { 
        switch(distanceSelection.value) {
            case "60":
            case "120":
                // Get all the events with a distance of less than 120
                const distanceEvents = this.events.filter(
                    e => e.distance() <= 120
                )

                //Get all the elements
                const d = Array.from(document.querySelectorAll('.event'))
                
                // Print the id number of each element to the console
                d.forEach(e => {
                    console.log(e.dataset.id)
                })
                
                //const finalList = Array.from(elements).filter(
                //    e => e.id === finalList.dataset.id
                //)

                //console.log(distanceEvents)
            
                //const idEvents = distanceEvents.filter(
                //    e => e.id === //event element.dataset.id
                //)


            case "180":
            case "240":
            case "300":
            case "400":
            case "750":
            case "1000":
            case "Anywhere":

        }






        //if (distanceSelection.value === '120'){
        //    const event = this.events.filter(
        //        e => e.distance() <= 120
        //    )
        //    console.log(event)
        //    // need to match up event id with event id of html element
//
        //    const filteredEvents = this.event.filter (
        //        e => e.id === eventSelection.dataset.id
        //    )
        //    console.log(filteredEvents)
        //}
    }

    _toggleEvent(eInfo){
        const eAutox = document.querySelectorAll('.event--autox')
        const eTrackDay = document.querySelectorAll('.event--trackday')
        const eRace = document.querySelectorAll('.event--race')

        if (eventSelection.value === 'all'){
            eAutox.forEach(el => el.style.display='block')
            eRace.forEach(el => el.style.display='block')
            eTrackDay.forEach(el => el.style.display='block')
        }
        if (eventSelection.value === 'autox'){
            eAutox.forEach(el => el.style.display='block')
            eRace.forEach(el => el.style.display='none')
            eTrackDay.forEach(el => el.style.display='none')
        }
        if (eventSelection.value === 'trackday'){
            eTrackDay.forEach(el => el.style.display='block')
            eAutox.forEach(el => el.style.display='none')
            eRace.forEach(el => el.style.display='none')
        }
        if (eventSelection.value === 'race'){
            eRace.forEach(el => el.style.display='block')
            eAutox.forEach(el => el.style.display='none')
            eTrackDay.forEach(el => el.style.display='none')
        }
    }

    _renderEventMarker(event){
        L.marker(event.coords)
        .addTo(this.map)
        .bindPopup(
            L.popup({
            maxWidth: 250,
            minWidth: 100,
            autoClose: false,
            closeOnClick: true,
            closeButton: false,
            className: `${event.type}-popup`,
            })
        )
        .setPopupContent(`${event.type}`)
        .openPopup()

        let html = ``

        if (event.type === 'autox'){
            html=`<img src="autox.png" alt="autox icon" title="Autox icon"></img>`
        } 
        if (event.type === 'trackday') {
            html=`<img src="helmet.png" alt="helmet icon" title="Helmet icon"></img>`
        }
        if (event.type === 'race') {
            html=`<img src="flag.png" alt="checkered flag icon" title="Checkered Flag icon">`
        }

        const eventPopup = document.querySelectorAll(`.${event.type}-popup`)
        console.log(eventPopup)
        eventPopup.forEach((el) => el.insertAdjacentHTML('afterbegin', html))
    }

    _renderEvent(eInfo){
        let html = `
        <li class="event event--${eInfo.type}" data-id="${eInfo.id}">
        <h2 class="event_title">${eInfo.title}</h2>
            <div class="event_details">
                <span class="event_date">${eInfo.date}<br></span>
                <span class="event_organizer">${eInfo.organizer}<br></span>
                <span class="event_loc">${eInfo.location}</span>
            </div>
        </li>`
        calendar.insertAdjacentHTML('afterend', html)

        const eventCard = document.querySelector(`.event--${eInfo.type}`)
        eventCard.style.display='block'
        setTimeout(() => {
            eventCard.style.opacity='1', 
            eventCard.style.transition='opacity .3s'
        }, 100)
    }
}

class trackInfo{
    constructor(name, coords=[]) {
        this.name = name
        //this.trklength = trklength 
        this.coords = coords
        //this.elevation = elevation
        //this.tracklength = tracklength
    }

    distance() {
        var loc = L.latLng({
            lat: this.coords[0],
            lng: this.coords[1],
        })

        var homeLoc = L.latlng({
            lat: this.home[0],
            lng: this.home[1],
        })

        this.distance = homeLoc.distanceTo(loc)/1609.344
        return this.distance
    }
}

class tracksApp {
    //#map
    //#mapZoomlevel = 16
    map
    mapZoomlevel = 16
    tracks = []

    constructor() {
        this._getPosition()
        console.log(mapID)
        //this.trackpopups.addEventListener('click', event => {
        //    console.log('clicked')
        //    //console.log(event.target.closest('.race_track-popup'))
        //})
        btnClose.addEventListener('click', () => {
            modal.classList.add('hidden')
            overlay.classList.add('hidden')
            overlay.classList.remove('overlay__fadein')

            setTimeout(() => overlay.style.display = 'none', 400)
            modal.classList.remove('modal__fadein')
            setTimeout(() => modal.style.display = 'none', 400)
        })
        overlay.addEventListener('click', () => {
            modal.classList.add('hidden')
            overlay.classList.add('hidden')
            overlay.classList.remove('overlay__fadein')
    
            setTimeout(() => overlay.style.display = 'none', 400)
            modal.classList.remove('modal__fadein')
            setTimeout(() => modal.style.display = 'none', 1000)
        })
        mapID.addEventListener('click', (e) => {
            if (e.target.matches('.leaflet-popup-content-wrapper'))
                {
                    console.log('CONTENTWRAPEPR!!!', e.target)
                    //overlay.classList.remove('hidden')
                    modal.classList.remove('hidden');
                    setTimeout(() => overlay.style.display = 'block', 10)
                    setTimeout(() => overlay.classList.add('overlay__fadein'), 15)

                    setTimeout(() => modal.style.display = 'block', 10)
                    setTimeout(() => modal.classList.add('modal__fadein'), 15)
                }
        })


    }

    _getPosition() {
        const geoOptions = {
            enableHighAccuracy: true,
        }
        const loc = navigator.geolocation.getCurrentPosition(
            //position => console.log(position),
            this._loadMap.bind(this),
            function() {alert('Could not get your position')},
            geoOptions)
    }

    _loadMap(position) {
        document.body.style.backgroundImage = "none"
        const {latitude} = position.coords
        const {longitude} = position.coords
        const coords = [latitude, longitude]

        this.map = L.map('map').setView(coords, this.mapZoomlevel)
        L.tileLayer('https://{s}.tile.openstreetmap.fr/hot//{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);

        //const trck1 = new trackInfo('auto_club_speedway', [34.0850884,-117.4942667])
        //this.tracks.push(trck1)
        //console.log(this.tracks)

        this._getTracks()
        this.map.flyTo(coords, 9.5)
    }

    _getTracks() {
        this._renderTrackMarker.bind(this)

        //API call to get tracks
        const req = fetch('http://127.0.0.1/tracks').then(response => {return response.json()
        }).then(data =>{
            data.forEach(item => {
                let coords = []
                coords.push(item.lat,item.lng)
                const formatTracKName = (trackNametext) => {
                    let trackName = trackNametext.charAt(0).toUpperCase() + trackNametext.slice(1)
                    let trackNameArray = trackNametext.split("")
                    let underscoreChar = trackNameArray.indexOf('_')

                    //If the indexOf() method does not find the substring in string, it will return -1.
                    while (underscoreChar != -1) {
                        trackNameArray.splice(underscoreChar, 1, ' ')
                        underscoreChar = trackNameArray.indexOf('_', underscoreChar + 1)
                    }
                    return trackNameArray.join('')
                }

                const t = new trackInfo(formatTracKName(item.name), coords)
                this._renderTrackMarker(t)
            })
        })
        
    }

    _showModal() {

    }

    _renderTrack() {}
    
    _renderTrackMarker(track){
        L.marker(track.coords)
        .addTo(this.map)
        .bindPopup(
            L.popup({
            maxWidth: 250,
            minWidth: 100,
            autoClose: false,
            closeOnClick: true,
            closeButton: false,
            //className: `${track.name}-popup`,
            className: `race_track-popup`,
            })
        )
        .setPopupContent(`${track.name}`)
        .openPopup()       
    }
}


class AutoX {

     //const meters = obj0.distanceTo(obj2)
    //console.log("DISTANCE FROM HOME TO AUTOX",meters)
    type = 'autox'
    title = 'NNJR Autocross Event #6 - Lot E'
    organizer = 'SCCA - Northern New Jersey Region'
    date = 'Sat, Jun 5, 2021'
    location = 'MetLife Stadium East Rutherford, NJ'
    coords = [40.7619505,-73.9331842]
}
class Race { 
    type = 'race'
    title = 'NASA at New Jersey Motorsports Park - Thunderbolt'
    organizer = 'NASA - Northeast Region'
    date = 'Fri, Jun 4 - Sun, Jun 6, 2021'
    location = 'NJ Motorsports Park Milleville, NJ'
    coords = [40.7619505,-73.9331842]
}
class TrackDay {
    type = 'trackday'
    title = 'SCDA- Lime Rock Park- Track Day Event- June 10th'
    organizer = 'Sports Car Driving Association-SCDA'
    date = 'Thu, Jun 10, 2021'
    location = 'Lime Rock Park Lakeville, CT'
    coords = [41.9295642,-73.3908371]
}
